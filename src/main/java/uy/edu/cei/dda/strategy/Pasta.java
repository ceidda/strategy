package uy.edu.cei.dda.strategy;

public class Pasta {

	private Tipo tipo;
	private Boolean cocida;

	public Pasta(Tipo tipo) {
		this.tipo = tipo;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Boolean getCocida() {
		return cocida;
	}

	public void setCocida(Boolean cocida) {
		this.cocida = cocida;
	}

}
