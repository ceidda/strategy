package uy.edu.cei.dda.strategy;

public abstract class FabricaDePastaTemplate {

	public static void main(String... args) {
		FabricaDePastaTemplate f = new FabricaDeTallarines();
		f.fabricar(Tipo.TALLARIN);
	}
	
	static final class FabricaDeTallarines extends FabricaDePastaTemplate {
		
		public FabricaDeTallarines() {
			super(10);
		}
		
		@Override
		protected void darForma(Pasta pasta) {
			System.out.println("dandole forma al tallarin");
		}
	
	}
	
	private final int tiempo;

	public FabricaDePastaTemplate(final int tiempo) {
		this.tiempo = tiempo;
	}

	public final Pasta fabricar(final Tipo tipo) {
		final Pasta pasta = new Pasta(tipo);
		this.darForma(pasta);
		this.cocinar(pasta);
		return pasta;
	}

	protected abstract void darForma(final Pasta pasta);

	private void cocinar(final Pasta pasta) {
		System.out.println("tiempo de coccion " + tiempo);
		pasta.setCocida(true);
	}
}
